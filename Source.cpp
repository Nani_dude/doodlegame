#include <SFML\Graphics.hpp>

bool GAMEOVER;
const float scaling = 0.8;
const int W_Height = 850 ;
const int W_Width = 532;
const int START_POSITION_y = W_Height*scaling/2;
const int START_POSITION_x = W_Width*scaling/2;

	int main(){
		sf::Sprite doodler;
		sf::Sprite platform;
		sf::Sprite background;
		
			sf::RenderWindow window(sf::VideoMode(W_Width * scaling, W_Height * scaling), "Doodle Jump v.0.1(alpha)");
			sf::Image background_image;
			background_image.loadFromFile("photo/background.png");
			sf::Image doodler_image;
			doodler_image.loadFromFile("photo/doodler.png");
			sf::Image platform_image;
			platform_image.loadFromFile("photo/platform.png");
			//�������� �������
			sf::Texture background_texture;
			background_texture.loadFromImage(background_image);
			sf::Texture doodler_texture;
			doodler_texture.loadFromImage(doodler_image);
			sf::Texture platform_texture;
			platform_texture.loadFromImage(platform_image);
			background.setTexture(background_texture);
			doodler.setTexture(doodler_texture);
			platform.setTexture(platform_texture);
			while (window.isOpen())
			{
				sf::Event event;
				while (window.pollEvent(event))
				{
					if (event.type == sf::Event::Closed)
						window.close();
				}

				window.clear();
				background.setPosition(0, 0);
				window.draw(background);
				doodler.setPosition(START_POSITION_x, START_POSITION_y);
				window.draw(doodler);
				
				window.display();
			};


		
	
	return 0;
}


